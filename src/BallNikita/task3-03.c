#include <stdio.h>
#define N 255
int main()
{
    int avt;
    char str[100];
    int arr[N] = { 0 };

    gets(str);
    avt = 0;
    while (str[avt])
    {
        arr[(int)str[avt] + 127]++;
        avt++;
    }

    for (avt = 0; avt < N; avt++)
    {
        if (arr[avt] != 0)
            printf("%c - %d\n", avt - 127, arr[avt]);
    }

    return 0;
}