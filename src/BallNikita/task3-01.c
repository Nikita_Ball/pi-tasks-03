#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 8

int main()
{
    int i, j, num = 0, sum, buf, nine = 0;
    long long int sumofnum[9 * N + 1] = { 0 };
    clock_t start, finish;
    double time;

    printf("Enter bits your number in [1..8] >> ");
    scanf("%d", &nine);
    if (nine <= 0 || nine>8)
    {
        puts("Incorrect data \n");
        return 404;
    }
    start = clock();
    for (i = 0; i<nine; i++)
        num = num * 10 + 9;
    for (i = 0; i <= num; i++)
    {
        sum = 0;
        buf = i;
        while (buf)
        {
            sum += (buf % 10);
            buf /= 10;
        }
        sumofnum[sum] += 1;
    }
    finish = clock();
    time = (double)finish - start / CLOCKS_PER_SEC;
    for (i = 0; i <= nine * 9; i++)
        printf("summ num for %d = %lld\n", i, sumofnum[i]);
    puts("");
    printf("time: %d  \n", time);
    return 0;
}