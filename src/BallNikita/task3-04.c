#include <stdio.h>
#include <time.h>



void maxnum(int num, int *max)
{
    if (num > *max)
        *max = num;
}


void minnum(int num, int *min)
{
    if (num < *min)
        *min = num;
}


double average(int digits)
{
    double av = digits / 25.0;
    printf("average = %f \n", av);
}



int main()
{
   
    srand(time(NULL));
    int max = -101;
    int min = 101;
    int string[25];
    double avt = 0.0;
    
    
    for (int i = 0; i < 25; i++)
    {
        string[i] = rand() % (100 - (-100)) + (-100);
        maxnum(string[i], &max);
        minnum(string[i], &min);
        avt += string[i];
        printf("%i ", string[i]);
    }
    
    puts("");
    printf("max = %i \n", max);
    printf("min = %i \n", min);
    average(avt);
    return 0;

}