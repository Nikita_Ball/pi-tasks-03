#include <stdio.h>

int main() 
{
    const int N = 255;
    int i;
    char str[100];
    int arr[N];
    
    
    for (i = 0; i < N; i++) 
    {
        arr[i] = 0;
    }

    gets(str);
    i = 0;
    while (str[i]) 
    {
        arr[(int)str[i] + 127]++;
        i++;
    }

    for (i = 0; i < N; i++) 
    {
        if (arr[i] != 0)
            printf("%c - %d\n", i - 127, arr[i]);
    }

    return 0;
}